using System;

namespace HelloWorld 
{
  
  class Variable
  {
  static void Main(string[] args)
  {
   //must be initalized 
    byte number= 255;
    float total = 20.42f;
    char charac = 'A';
    string firstName = "kssa";
    bool isWorking = true; 
  
    Console.WriteLine(number);
    Console.WriteLine(total);
    Console.WriteLine(firstName);
    Console.WriteLine(isWorking);
    Console.WriteLine(charac);
    
    //format string
    
    Console.WriteLine( "{0} {1}", byte.MinValue, byte.MaxValue);
    Console.WriteLine( "{0} {1}", float.MinValue, float.MaxValue);
    
    //
    const float Pi = 3.14f;
    Pi = 1;
    

    
  }  
    
    
  }
  
  
}